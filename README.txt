
Upload Package
------------------------
By: Shawn Conn (http://www.shawnconn.com/contact)

Upload Package module allows you upload modules through Drupal's administration
interface. It enables a new task on the Drupal module and theme administration 
page. This form will let you enter a URL containing the project package tarball 
or enter a tarball file to upload. This module is dependent on PEAR's
Archive_Tar package (http://pear.php.net/package/Archive_Tar).

Installation Instructions:
1. Install PEAR if it isn't already installed. 
(instructions at http://pear.php.net/manual/en/installation.php)
2. Download the PEAR Archive_Tar package and place the Archive directory in path
that's included in PHP's include_path.
(located at http://pear.php.net/package/Archive_Tar)
3. Copy the Upload Package module to your module directory and enable it per 
usual.
4. Optional, set the directory where you want modules or themes unpacked to by 
browsing to admin/settings/upload_package.
